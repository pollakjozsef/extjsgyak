
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'Ext.MessageBox', 'MyApp.view.main.WestPane'
    ],
    refs: [{
            ref: 'west',
            selector: 'mywestpanel'
        }],
    view: ['Ext.container.Container', 'MyApp.view.main.WestPane'],
    alias: 'controller.main',
    onClickButton: function () {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },
    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    init: function () {
        //console.log(getWest());
        var me = this;
        me.control({
            'mywestpanel': {
                afterrender: function ()
                {
                    console.log('Westpanel activated!');
                    me.getWest();
                }
            }
        });
        //this.startSocket();
    },
    startSocket: function startSocket() {
        console.log('elindultaaam');
        var socket = new io('http://localhost:6969');
        socket.on('news', function (data) {
            console.log(data);
            console.log(this.getWest());
//            app.west().items.push({
//                xtype: 'button',
//                title: 'data'
//            });
        });
    }
});



